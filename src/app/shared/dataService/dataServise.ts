import {BehaviorSubject} from 'rxjs';

export class DataService {

  //////////// isLogin ////////////
  public static isLogin = new BehaviorSubject<boolean>(false);
  public static getIsLogin = DataService.isLogin.asObservable();

  public static setIsLogin(value: boolean) {
    DataService.isLogin.next(value);
  }


}

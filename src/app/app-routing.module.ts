import {NgModule} from '@angular/core';
import {AuthGuardService} from './core/services/gard/auth-guard.service';
import {PanelGuardService} from './core/services/gard/panel-guard.service';
import {NotfoundComponent} from './pages/notfound/notfound.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: '/auth', pathMatch: 'full'},
    {
        path: 'auth', loadChildren: () => import('./pages/auth/auth.module').then((m) => m.AuthModule)
        , canActivate: [AuthGuardService]
    },
    {
        path: 'panel', loadChildren: () => import('./pages/_panel/panel.module').then((m) => m.PanelModule)
        , canActivate: [PanelGuardService]
    },

    {path: 'Notfound', component: NotfoundComponent},
    {path: '**', redirectTo: '/Notfound'},
];




@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true, paramsInheritanceStrategy: 'always'})],
    exports: [RouterModule],
})
export class AppRoutingModule {
}

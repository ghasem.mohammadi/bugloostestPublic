import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {BaseApiService} from 'src/app/core/services/base-api.service';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    private readonly VERIFICATION_TOKEN_VERIFICAITON_API_PATH: string = `${environment.serverApiPath}/users`;

    constructor(private baseApiService: BaseApiService) {}

    confirmVerificationToken(verificationTokenConfirmation: any) {
        return this.baseApiService
            .post<number, any>(
                this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
                verificationTokenConfirmation
            )
            .pipe(
                tap((data) => {
                    return data;
                })
            );
    }


}

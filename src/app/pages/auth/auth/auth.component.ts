import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Subscription} from 'rxjs';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
    private subscription$: Subscription[] = [];

    constructor(private authService: AuthService,
                private router: Router) {
    }

    ngOnInit(): void {
    }


    loginAdmin1() {
        localStorage.setItem('token', 'Admin1');
        this.router.navigate(['/panel'], {queryParams: {}});

    }
    loginAdmin2() {
        localStorage.setItem('token', 'Admin2');
        this.router.navigate(['/panel'], {queryParams: {}});

    }

    loginUser() {
        localStorage.setItem('token', 'User');
        this.router.navigate(['/panel'], {queryParams: {}});

    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());
    }
}

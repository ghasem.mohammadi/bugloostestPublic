import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProcessModel} from '../../model/process';
import {Form} from '../../../form-builder/model/form';
import {FormService} from '../../../form-builder/service/form.service';
import {Subscription} from 'rxjs';
import {ActionMode} from '../../../../shared/model/action-mode';
import {ProcessService} from '../../service/process.service';
import {Tools} from '../../../../shared/dataService/Tools';

@Component({
    selector: 'app-process-levels',
    templateUrl: './process-levels.component.html',
    styleUrls: ['./process-levels.component.scss']
})
export class ProcessLevelsComponent implements OnInit, OnDestroy {
    @Input() process = {levelList: []} as ProcessModel.Process;
    @Input() mode: ActionMode;
    levelForEdit = {} as ProcessModel.Level;
    // };
    formList: Form[] = [];
    loadingFormList: boolean;
    editActivityStatus: boolean;
    private subscription$: Subscription[] = [];

    constructor(private formService: FormService,
                private processService: ProcessService) {
    }

    ngOnInit(): void {
        this.getFormList();
    }

    getFormList() {
        this.loadingFormList = true;
        this.subscription$.push(this.formService.getList().subscribe((res) => {
                this.loadingFormList = false;
                if (res) {
                    this.formList = res;
                }
            })
        );
    }

    editLevel(item) {
        this.levelForEdit = JSON.parse(JSON.stringify(item));
    }

    addLevel() {
        this.levelForEdit.title = this.levelForEdit.title.trim();
        this.levelForEdit.description = this.levelForEdit.description.trim();
        if (!this.levelForEdit.title) {
            alert('عنوان وارد شود');
            return;

        }
        if (!this.levelForEdit.formId) {
            alert('فرم انتخاب  شود');
            return;

        }
        if (this.levelForEdit.id) {
            const index = this.process.levelList.findIndex(l => l.id === this.levelForEdit.id);
            this.process.levelList[index] = (JSON.parse(JSON.stringify(this.levelForEdit)));
        } else {
            this.levelForEdit.id = Tools.randomId();
            this.process.levelList.push(JSON.parse(JSON.stringify(this.levelForEdit)));
        }
        this.levelForEdit = {} as ProcessModel.Level;
        this.updateProcess();
    }

    cancelEdit() {
        this.levelForEdit = {} as ProcessModel.Level;

    }

    delete(index) {

        this.process.levelList.splice(index, 1);
        this.updateProcess();

    }

    elementMove(type: string, levelIndex: number) {
        const thisLevel = JSON.parse(JSON.stringify(this.process.levelList[levelIndex]));
        if (type === 'up') {
            this.process.levelList[levelIndex] = JSON.parse(JSON.stringify(this.process.levelList[levelIndex - 1]));
            this.process.levelList[levelIndex - 1] = thisLevel;
        } else if (type === 'down') {
            this.process.levelList[levelIndex] = JSON.parse(JSON.stringify(this.process.levelList[levelIndex + 1]));
            this.process.levelList[levelIndex + 1] = thisLevel;
        }
        this.updateProcess();

    }

    updateProcess() {
        this.processService.update(this.process, this.process.id).subscribe();

    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

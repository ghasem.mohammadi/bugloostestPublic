import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessLevelsComponent } from './process-levels.component';

describe('ProcessLevelsComponent', () => {
  let component: ProcessLevelsComponent;
  let fixture: ComponentFixture<ProcessLevelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcessLevelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

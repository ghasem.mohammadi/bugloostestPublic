import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessUpsertComponent } from './process-upsert.component';

describe('ProcessUpsertComponent', () => {
  let component: ProcessUpsertComponent;
  let fixture: ComponentFixture<ProcessUpsertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcessUpsertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

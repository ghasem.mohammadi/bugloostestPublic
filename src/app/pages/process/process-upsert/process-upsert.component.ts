import {Component, OnDestroy, OnInit} from '@angular/core';
import {Form} from '../../form-builder/model/form';
import {ProcessModel} from '../model/process';
import {ActionMode} from '../../../shared/model/action-mode';
import {ActivatedRoute, Router} from '@angular/router';
import {ProcessService} from '../service/process.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-process-upsert',
    templateUrl: './process-upsert.component.html',
    styleUrls: ['./process-upsert.component.scss']
})
export class ProcessUpsertComponent implements OnInit, OnDestroy {
    process = {} as ProcessModel.Process;
    mode: ActionMode;
    actionMode = ActionMode;
    entityId: string;
    private subscription$: Subscription[] = [];

    constructor(public router: Router,
                private entityService: ProcessService,
                private  activatedRoute: ActivatedRoute) {
        this.entityId = this.activatedRoute.snapshot.queryParams.entityId;
        this.mode = this.activatedRoute.snapshot.queryParams.mode;

    }

    ngOnInit(): void {
        if (this.entityId) {
            this.getOne();
        }
    }


    getOne() {
        this.subscription$.push(this.entityService.getOne().subscribe((res) => {
            console.log(res);
            this.process = res.find(d => d.id === this.entityId);
        }));
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ActionMode} from '../../../../shared/model/action-mode';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MyPattern} from '../../../../shared/model/myPattern';
import {ProcessModel} from '../../model/process';
import {ProcessService} from '../../service/process.service';
import {Tools} from '../../../../shared/dataService/Tools';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-process-information',
    templateUrl: './process-information.component.html',
    styleUrls: ['./process-information.component.scss']
})
export class ProcessInformationComponent implements OnInit, OnDestroy {
    @Input() process: ProcessModel.Process = {id: '', description: '', levelList: [], title: ''};
    @Input() mode: ActionMode;
    @Output() processIdOut = new EventEmitter<string>();
    actionMode = ActionMode;

    htmlForm: FormGroup;
    myPattern = MyPattern;
    submitted = false;

    loadingSubmit = false;

    ShowFormBody = false;
    private subscription$: Subscription[] = [];

    constructor(private processService: ProcessService) {
    }


    ngOnInit(): void {
        if (!this.process) {
            this.process = {id: '', description: '', levelList: [], title: ''};
        }
        this.creatForm();


    }

    creatForm() {
        this.htmlForm = new FormGroup({
            title: new FormControl({value: null}, Validators.required),
            grouping: new FormControl({value: null}),
            description: new FormControl({value: null})
        });
        if (this.mode === ActionMode.VIEW) {
            this.htmlForm.disable();
        }
        this.ShowFormBody = false;
        setTimeout(e => {
            this.ShowFormBody = true;
        }, .000000000000000000000000001);
    }


    onSubmit() {
        if (!this.htmlForm.dirty) {
            alert('تغییری اعمال نشده است.');
            return;
        }
        // if (this.loadingSubmit) {
        //   return;
        // }
        //
        // this.submitted = true;
        if (this.htmlForm.invalid) {
            alert('مقادیر خواسته شده را درست وارد کنید.');

            return;
        }
        if (this.process.id) {

            // this.loadingSubmit = true;
            this.subscription$.push(this.processService.update(this.process, this.process.id).subscribe());
            alert('با موفقیت ویرایش شد');

        } else {
            this.process.id = Tools.randomId();
            this.process.levelList = [];
            this.processIdOut.emit(this.process.id);
            this.subscription$.push(this.processService.create(this.process).subscribe());
            alert('با موفقیت ایجاد شد');

            console.log(this.process);

        }
    }

    back() {
        window.history.back();
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {TokenModel} from '../../../shared/model/token-model';
import {ProcessModel} from '../model/process';
import {ActivatedRoute, Router} from '@angular/router';
import {ProcessService} from '../service/process.service';
import {ActionMode} from '../../../shared/model/action-mode';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-process-list',
    templateUrl: './process-list.component.html',
    styleUrls: ['./process-list.component.scss']
})
export class ProcessListComponent implements OnInit, OnDestroy {
    token = localStorage.getItem('token');
    TokenModel = TokenModel;
    entityList: ProcessModel.Process[] = [];
    loading = false;
    private subscription$: Subscription[] = [];

    constructor(
        private entityService: ProcessService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.getPage();

        });
    }

    ngOnInit(): void {
    }

    getPage() {
        this.loading = true;
        this.subscription$.push(this.entityService.getList().subscribe((res) => {
                this.loading = false;
                if (res) {
                    this.entityList = res;
                }
            })
        );
    }

    chooseSelectedItemForEdit(item: ProcessModel.Process) {

        this.router.navigate(['upsert'], {
            queryParams: {mode: ActionMode.EDIT, entityId: item.id},
            relativeTo: this.activatedRoute
        });

    }

    viewAnswerForProcess(item: ProcessModel.Process) {
        this.router.navigate(['viewAnswerForProcess'], {
            queryParams: { entityId: item.id},
            relativeTo: this.activatedRoute
        });
    }

    runProcess(id: string) {
        this.router.navigate(['runProcess'], {
            queryParams: { entityId: id},
            relativeTo: this.activatedRoute
        });
    }


    deleteItem(form, i) {
        this.entityService.delete(form.id).subscribe(() => {
            this.entityList.splice(i, 1);
        });
    }


    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }

}

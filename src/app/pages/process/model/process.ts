export namespace ProcessModel {

    export interface Process {
        id: string;
        title: string;
        description: string;
        levelList: Level[];
    }

    export interface Level {
        id: string;
        title: string;
        description: string;
        formId: string;
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProcessListComponent} from './process-list/process-list.component';
import {ProcessUpsertComponent} from './process-upsert/process-upsert.component';
import {RunProcessComponent} from './run-process/run-process.component';
import {ViewAnswerForProcessComponent} from './view-answer-for-process/view-answer-for-process.component';

const routes: Routes = [
    {
        path: '',
        component: ProcessListComponent
    },
    {
        path: 'upsert',
        component: ProcessUpsertComponent
    },
    {
        path: 'runProcess',
        component: RunProcessComponent
    },
    {
        path: 'viewAnswerForProcess',
        component: ViewAnswerForProcessComponent,

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProcessRoutingModule {
}

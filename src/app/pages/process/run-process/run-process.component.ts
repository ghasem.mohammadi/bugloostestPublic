import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProcessService} from '../service/process.service';
import {Subscription} from 'rxjs';
import {ProcessModel} from '../model/process';
import {ActivatedRoute} from '@angular/router';
import {ProcessAnswerService} from '../service/processAnswer.service';
import {FormData} from '../../form-builder/model/formData';

@Component({
    selector: 'app-run-process',
    templateUrl: './run-process.component.html',
    styleUrls: ['./run-process.component.scss']
})
export class RunProcessComponent implements OnInit, OnDestroy {

    entityId: string;
    private subscription$: Subscription[] = [];
    process = {} as ProcessModel.Process;
    tabIndex = 0;
    formDataList = [];

    constructor(private entityService: ProcessService,
                private processAnswerService: ProcessAnswerService,
                private  activatedRoute: ActivatedRoute) {
        this.entityId = this.activatedRoute.snapshot.queryParams.entityId;
    }

    ngOnInit(): void {
        if (this.entityId) {
            this.getOne();
        }
    }


    getOne() {
        this.subscription$.push(this.entityService.getOne().subscribe((res) => {
            console.log(res);
            this.process = res.find(d => d.id === this.entityId);
        }));
    }

    public getFormData(event: FormData, i) {
        this.formDataList.push(event);
        if (this.tabIndex === this.process.levelList.length - 1) {
            this.processAnswerService.create(this.formDataList, this.process.id).subscribe((res) => {
                alert('با موفقیت به اتمام رسید.');
                window.history.back();
            });
        } else {
            this.tabIndex++;
        }
    }

    back(event: boolean) {
        if (this.tabIndex < this.process.levelList.length - 1) {
            if (confirm('فرآیند کامل نشده اشت ')) {
                window.history.back();
            }
        } else {
            window.history.back();

        }
    }

    ngOnDestroy(): void {

        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

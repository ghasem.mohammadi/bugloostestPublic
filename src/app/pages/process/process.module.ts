import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcessRoutingModule } from './process-routing.module';
import { ProcessListComponent } from './process-list/process-list.component';
import { ProcessUpsertComponent } from './process-upsert/process-upsert.component';
import { ProcessInformationComponent } from './process-upsert/process-information/process-information.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProcessLevelsComponent } from './process-upsert/process-levels/process-levels.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { RunProcessComponent } from './run-process/run-process.component';
import { ViewAnswerForProcessComponent } from './view-answer-for-process/view-answer-for-process.component';
import {FormBuilderModule} from '../form-builder/form-builder.module';


@NgModule({
  declarations: [
    ProcessListComponent,
    ProcessUpsertComponent,
    ProcessInformationComponent,
    ProcessLevelsComponent,
    RunProcessComponent,
    ViewAnswerForProcessComponent
  ],
  imports: [
    CommonModule,
    ProcessRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
    FormBuilderModule
  ]
})
export class ProcessModule { }

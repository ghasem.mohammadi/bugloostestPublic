import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProcessAnswerService} from '../service/processAnswer.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-view-answer-for-process',
    templateUrl: './view-answer-for-process.component.html',
    styleUrls: ['./view-answer-for-process.component.scss']
})
export class ViewAnswerForProcessComponent implements OnInit, OnDestroy {
    entityId: string;
    entityList = [];
    index = 0;
    tabIndex = 0;
    private subscription$: Subscription[] = [];

    constructor(private processAnswerService: ProcessAnswerService, private  activatedRoute: ActivatedRoute) {
        this.entityId = this.activatedRoute.snapshot.queryParams.entityId;
    }

    ngOnInit(): void {
        this.subscription$.push(this.processAnswerService.getOne(this.entityId).subscribe((res) => {
            console.log(res);
            if (res) {
                this.entityList = res;
            } else {
                alert('برای این فرایند پاسخی وارد نشده');
                window.history.back();
            }

        }));
    }

    public selectTabIndex(type: string) {
        if (type === 'next') {
            this.tabIndex++;

        } else {
            this.tabIndex--;

        }
    }

    public selectIndex(type: string) {
        if (type === 'next') {
            this.index++;

        } else {
            this.index--;

        }
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

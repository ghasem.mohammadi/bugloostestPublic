import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAnswerForProcessComponent } from './view-answer-for-process.component';

describe('ViewAnswerForProcessComponent', () => {
  let component: ViewAnswerForProcessComponent;
  let fixture: ComponentFixture<ViewAnswerForProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAnswerForProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAnswerForProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

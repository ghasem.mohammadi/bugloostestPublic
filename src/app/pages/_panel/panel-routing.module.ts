import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PanelComponent} from './panel/panel.component';

const routes: Routes = [
    {
        path: '', component: PanelComponent, children: [
            {path: '', redirectTo: 'formBuilder', pathMatch: 'full'},
            {
                path: 'formBuilder', loadChildren: () => import('../form-builder/form-builder.module').then((m) => m.FormBuilderModule)
            },
            {
                path: 'process', loadChildren: () => import('../process/process.module').then((m) => m.ProcessModule)
            },

        ]
    },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PanelRoutingModule {
}

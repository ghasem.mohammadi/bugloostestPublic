import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PanelRoutingModule} from './panel-routing.module';
import { PanelComponent } from './panel/panel.component';
import {TemplateModule} from '../template/template.module';


@NgModule({
    declarations: [
    PanelComponent
  ],
    imports: [
        CommonModule,
        PanelRoutingModule,
        TemplateModule
    ],
    providers: []
})
export class PanelModule {
}

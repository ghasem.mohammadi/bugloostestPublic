import {Element} from './element';

export interface Form {
    id: string;
    title: string;
    description: string;
    elementList: Element[];
}

import {ElementType} from './enum/element-type';

export interface FormData {
    id: string;
    formId: string;
    answerList: Array<QuestionAnswer>;
}

export interface QuestionAnswer {
    questionId: string;
    questionElementType: ElementType;
    answerIdList: Array<any>; // Answers of all questions are array of string except matrix.
}

export interface DateInputObject {
    id: string;
    date: any;
}

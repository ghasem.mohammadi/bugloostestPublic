import {ElementType} from './enum/element-type';
import {Option} from './option';

export interface Element {
    id: string;
    questionTitle: string;
    guide: string;
    required: boolean;
    placeHolder: string;
    pattern: string;
    dir: string;
    mask: string;
    maxLength: number;
    minLength: number;
    newElementType: ElementType;
    maxItemSelectable: number;
    minItemSelectable: number;
    newOptionList: Option[];
}

export enum ActionMode {
  ADD = 'ADD',
  EDIT = 'EDIT',
  VIEW = 'VIEW'
}

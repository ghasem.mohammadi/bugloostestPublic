export enum ElementType {
    TEXT_FIELD = 'TEXT_FIELD' as any,
    CHECK_BOX = 'CHECK_BOX' as any,
    TEXT_AREA = 'TEXT_AREA' as any,
    COMBO_BOX = 'COMBO_BOX' as any,
    RADIO_BUTTON = 'RADIO_BUTTON' as any,
    NUMERICAL = 'NUMERICAL' as any,
    DATE = 'DATE' as any,
    DATE_RANGE = 'DATE_RANGE' as any,
    TIME = 'TIME' as any,
}

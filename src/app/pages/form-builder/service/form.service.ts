import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {BaseApiService} from '../../../core/services/base-api.service';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  // private readonly VERIFICATION_TOKEN_VERIFICAITON_API_PATH: string = `${environment.serverApiPath}/form`;
  private readonly VERIFICATION_TOKEN_VERIFICAITON_API_PATH: string = `form`;


  constructor(private baseApiService: BaseApiService) {
  }


  create(form) {
    console.log('form', form);

    return this.baseApiService
      .post<number, any>(
        this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
        form
      )
      .pipe(
        tap((data) => {
          return data;
        })
      );
  }

  update(form, id) {
    return this.baseApiService
      .put<number, any>(
        this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
        form, id
      )
      .pipe(
        tap((data) => {
          return data;
        })
      );
  }

  getList(params?: any) {
    return this.baseApiService
      .get<any[]>(
        this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
        params
      )
      .pipe(
        tap((data) => {
          return data;
        })
      );
  }

  getOne(params?: any) {
    return this.baseApiService
      .get<any[]>(
        this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
      )
      .pipe(
        tap((data) => {
          return data;
        })
      );
  }

  delete(id) {
    return this.baseApiService
      .delete(
        this.VERIFICATION_TOKEN_VERIFICAITON_API_PATH,
        id
      )
      .pipe(
        tap((data) => {
          return data;
        })
      );
  }
}

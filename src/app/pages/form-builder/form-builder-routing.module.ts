import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UpsertFormComponent} from './action/upsert-form/upsert-form.component';
import {FormListComponent} from './form-list/form-list.component';
import {FormAnswerComponent} from './form-answer/form-answer.component';

const routes: Routes = [
    {
        path: '',
        component: FormListComponent
    },
    {
        path: 'upsert',
        component: UpsertFormComponent
    }, {

        path: 'view',
        component: UpsertFormComponent
    }, {

        path: 'answer',
        component: FormAnswerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormBuilderRoutingModule {
}

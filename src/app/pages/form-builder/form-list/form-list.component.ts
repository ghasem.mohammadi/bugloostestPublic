import {Component, OnDestroy, OnInit} from '@angular/core';
import {Form} from '../model/form';
import {FormService} from '../service/form.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionMode} from '../../../shared/model/action-mode';
import {TokenModel} from '../../../shared/model/token-model';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-form-list',
    templateUrl: './form-list.component.html',
    styleUrls: ['./form-list.component.scss']
})
export class FormListComponent implements OnInit, OnDestroy {

    loading: boolean;
    entityList: Form[] = [];
    actionMode = ActionMode;

    formForCopyId: string;
    loadingCopy = false;
    token = localStorage.getItem('token');
    TokenModel = TokenModel;
    private subscription$: Subscription[] = [];

    constructor(
        private entityService: FormService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.queryParams.subscribe((params) => {
            this.getPage();

        });
    }


    ngOnInit(): void {
    }

    getPage() {
        this.loading = true;
        this.subscription$.push(this.entityService.getList().subscribe((res) => {
                this.loading = false;
                if (res) {
                    this.entityList = res;
                }
            })
        );
    }

    chooseSelectedItemForEdit(item: Form) {

        this.router.navigate(['upsert'], {
            queryParams: {mode: ActionMode.EDIT, entityId: item.id},
            relativeTo: this.activatedRoute
        });

    }

    chooseSelectedItemForView(item: Form) {
        this.router.navigate(['view'], {
            queryParams: {mode: ActionMode.VIEW, entityId: item.id},
            relativeTo: this.activatedRoute
        });
    }

    chooseSelectedItemForAnswer(formId: string) {
        this.router.navigate(['answer'], {
            queryParams: {formId},
            relativeTo: this.activatedRoute
        });
    }


    deleteItem(form, i) {
        this.entityService.delete(form.id).subscribe(() => {
            this.entityList.splice(i, 1);
        });
    }


    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}



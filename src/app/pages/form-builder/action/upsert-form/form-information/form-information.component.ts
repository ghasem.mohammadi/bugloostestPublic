import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Form} from '../../../model/form';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../../service/form.service';
import {ActionMode} from '../../../../../shared/model/action-mode';
import {MyPattern} from '../../../../../shared/model/myPattern';
import {Tools} from '../../../../../shared/dataService/Tools';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-form-information',
    templateUrl: './form-information.component.html',
    styleUrls: ['./form-information.component.scss']
})
export class FormInformationComponent implements OnInit, OnDestroy {


    @Input() form: Form = {id: '', description: '', elementList: [], title: ''};
    @Input() mode: ActionMode;
    @Output() formIdOut = new EventEmitter<string>();
    actionMode = ActionMode;

    htmlForm: FormGroup;
    myPattern = MyPattern;
    submitted = false;

    loadingSubmit = false;

    ShowFormBody = false;
    private subscription$: Subscription[] = [];

    constructor(private formService: FormService) {
    }

    ngOnInit(): void {
        if (!this.form) {
            this.form = {id: '', description: '', elementList: [], title: ''};
        }
        this.creatForm();


    }

    creatForm() {
        this.htmlForm = new FormGroup({
            title: new FormControl({value: null}, Validators.required),
            grouping: new FormControl({value: null}),
            description: new FormControl({value: null})
        });
        if (this.mode === ActionMode.VIEW) {
            this.htmlForm.disable();
        }
        this.ShowFormBody = false;
        setTimeout(e => {
            this.ShowFormBody = true;
        }, .000000000000000000000000001);
    }


    onSubmit() {
        if (!this.htmlForm.dirty) {
            alert('تغییری اعمال نشده است.');
            return;
        }
        // if (this.loadingSubmit) {
        //   return;
        // }
        //
        // this.submitted = true;
        if (this.htmlForm.invalid) {
            alert('مقادیر خواسته شده را درست وارد کنید.');

            return;
        }
        if (this.form.id) {

            // this.loadingSubmit = true;
            this.subscription$.push(this.formService.update(this.form, this.form.id).subscribe());
            alert('با موفقیت ویرایش شد');

        } else {
            this.form.id = Tools.randomId();
            this.formIdOut.emit(this.form.id);
            this.subscription$.push(this.formService.create(this.form).subscribe());
            alert('با موفقیت ایجاد شد');

            console.log(this.form);

        }
    }

    back() {
        window.history.back();
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Form} from '../../model/form';
import {FormService} from '../../service/form.service';
import {ActionMode} from '../../../../shared/model/action-mode';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-upsert-form',
    templateUrl: './upsert-form.component.html',
    styleUrls: ['./upsert-form.component.scss']
})
export class UpsertFormComponent implements OnInit ,OnDestroy {
    entityId: string;
    form: Form = {
        id: '',
        title: '',
        elementList: [],
        description: ''
    };
    mode: ActionMode;
    actionMode = ActionMode;
    private subscription$: Subscription[] = [];

    constructor(public router: Router,
                private entityService: FormService,
                private  activatedRoute: ActivatedRoute) {
        this.entityId = this.activatedRoute.snapshot.queryParams.entityId;
        this.mode = this.activatedRoute.snapshot.queryParams.mode;
    }

    ngOnInit(): void {
        if (this.entityId) {
            this.getOne();
        }
    }

    getOne() {
        this.subscription$.push(this.entityService.getOne().subscribe((res) => {
            console.log(res);
            this.form = res.find(d => d.id === this.entityId);
        }));
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }

}

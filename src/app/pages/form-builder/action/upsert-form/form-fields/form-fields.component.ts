import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Form} from '../../../model/form';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Element} from '../../../model/element';
import {ElementType} from '../../../model/enum/element-type';
import {Option} from '../../../model/option';
import {FormService} from '../../../service/form.service';
import {Location} from '@angular/common';
import {ActionMode} from '../../../../../shared/model/action-mode';
import {MyPattern} from '../../../../../shared/model/myPattern';
import {TokenModel} from '../../../../../shared/model/token-model';
import {Tools} from '../../../../../shared/dataService/Tools';
import {Subscription} from 'rxjs';

declare var $: any;

@Component({
    selector: 'app-form-fields',
    templateUrl: './form-fields.component.html',
    styleUrls: ['./form-fields.component.scss']
})
export class FormFieldsComponent implements OnInit, AfterViewInit, OnDestroy {


    @Input() form: Form;
    @Input() mode: ActionMode;
    actionMode = ActionMode;
    myPattern = MyPattern;
    htmlForm: FormGroup;
    // newElement: Element;
    newElement = {
        newOptionList: [],
        dir: 'rtl',
    } as Element;
    newOption = {
        value: '',
        id: ''
    } as Option;
    submitted = false;
    newElementType = ElementType;
    newElementListCopy: Element[];
    loadingSubmit = false;
    token = localStorage.getItem('token');
    TokenModel = TokenModel;
    directionList = [
        {title: 'راست چین', value: 'rtl'},
        {title: 'چپ چین ', value: 'ltr'}
    ];
    requiredList = [
        {title: 'اجباری', value: true},
        {title: 'اختیاری ', value: false}
    ];

    newElementTypeList = [
        {title: 'متن کوتاه', id: ElementType.TEXT_FIELD},
        {title: 'عدد', id: ElementType.NUMERICAL},
        {title: 'متن بلند(توضیحی)', id: ElementType.TEXT_AREA},
        {title: 'لیستی', id: ElementType.COMBO_BOX},
        {title: 'گزینه ای چند انتخابی', id: ElementType.CHECK_BOX},
        {title: 'گزینه ای تک انتخابی', id: ElementType.RADIO_BUTTON},
        {title: 'تاریخ', id: ElementType.DATE},
        {title: 'بازه تاریخی', id: ElementType.DATE_RANGE},
        {title: 'ساعت', id: ElementType.TIME}
    ];
    private subscription$: Subscription[] = [];

    constructor(private formService: FormService,
                public location: Location,
    ) {
    }

    ngOnInit(): void {

        if (this.form.elementList) {
            this.newElementListCopy = JSON.parse(JSON.stringify(this.form.elementList));
        }
        this.creatForm();
    }

    creatForm() {
        this.htmlForm = new FormGroup({
            questionTitle: new FormControl({value: null}, Validators.required),
            guide: new FormControl({value: null}), // راهنما
            placeHolder: new FormControl({value: null}), // متن جانما
            pattern: new FormControl({value: null}), // فرمت پترن
            mask: new FormControl({value: null}), // فرمت مسک
            direction: new FormControl({value: null}), // فرمت  چینش
            newElementType: new FormControl({value: null}, Validators.required), //  نوع
            required: new FormControl({value: null}), // اجباری
            rangeSelector: new FormControl({value: false}), // بازه تاریخ
            maxLength: new FormControl({value: null}), //  حداکثر طول
            minLength: new FormControl({value: null}), //  حداقل طول
            maxItemSelectable: new FormControl({value: null}), //  حداکثر گزینه  قابل انتخاب
            minItemSelectable: new FormControl({value: null}), //  حداقل گزینه  قابل انتخاب
            newOptionList: new FormControl({value: null}), //  گزینه ها
            optionTitle: new FormControl({value: null}) //  گزینه هاعنوان
        });
        if (this.mode === ActionMode.VIEW) {
            this.htmlForm.disable();

        }
    }


    onSubmit() {
        // return;
        this.submitted = true;
        if (!this.newElement.questionTitle) {
            alert('عنوان وازد شود.');
            return;
        }
        if (!this.newElement.questionTitle) {
            alert('نوع انتخاب شود.');
            return;
        }


        if (this.newElement.newElementType === ElementType.TEXT_FIELD ||
            this.newElement.newElementType === ElementType.NUMERICAL ||
            this.newElement.newElementType === ElementType.TEXT_AREA ||
            this.newElement.newElementType === ElementType.DATE ||
            this.newElement.newElementType === ElementType.TIME
        ) {
            this.newElement.newOptionList = [];
            this.newElement.maxItemSelectable = null;
            this.newElement.minItemSelectable = null;
            if (!this.newElement.minLength) {
                this.newElement.minLength = 0;
            }
            if (!this.newElement.maxLength) {
                if (this.newElement.newElementType === ElementType.TEXT_AREA) {
                    this.newElement.maxLength = 512;
                }
                if (this.newElement.newElementType === ElementType.TEXT_FIELD) {
                    this.newElement.maxLength = 32;
                }
                if (this.newElement.newElementType === ElementType.NUMERICAL) {
                    this.newElement.maxLength = 1024;
                }
            }
        }
        if (this.newElement.newElementType === ElementType.DATE ||
            this.newElement.newElementType === ElementType.TIME
        ) {
            this.newElement.maxLength = null;
            this.newElement.minLength = null;
        }
        if (this.newElement.newElementType === ElementType.COMBO_BOX ||
            this.newElement.newElementType === ElementType.RADIO_BUTTON ||
            this.newElement.newElementType === ElementType.CHECK_BOX) {

            if (this.newElement.newOptionList.length === 0) {
                alert('حداقل یک گزینه انتخاب شود.');
                return;
            }

        }
        if (this.newElement.newElementType === ElementType.CHECK_BOX) {

            if ((this.newElement.minItemSelectable === null || this.newElement.minItemSelectable === undefined)) {
                alert('حداقل گزینه  قابل انتخاب  وارد شود.');
                return;
            }
            if ((this.newElement.maxItemSelectable === null || this.newElement.maxItemSelectable === undefined)) {
                alert('حداقل گزینه  قابل انتخاب  وارد شود.');
                return;
            }
            if (this.newElement.minItemSelectable > this.newElement.newOptionList.length) {
                alert('حداقل گزینه  قابل انتخاب نباید بیشتر از تعداد گزینه ها باشد.');
                return;
            }
            if (this.newElement.maxItemSelectable > this.newElement.newOptionList.length) {
                alert('حداکثر گزینه  قابل انتخاب نباید بیشتر از تعداد گزینه ها باشد.');
                return;
            }

        }
        if (this.newElement.maxLength < this.newElement.minLength) {
            alert('حداکثر طول نباید کوچکتر از حداقل طول باشد.');

            return;
        }
        if (this.newElement.maxItemSelectable < this.newElement.minItemSelectable) {
            alert('حداکثر گزینه  قابل انتخاب نباید کوچکتر از حداقل گزینه  قابل انتخاب باشد.');
            return;
        }
        if (!this.newElement.questionTitle) {
            alert('نوع انتخاب شود.');
            return;
        }
        if (this.newElement.id) {
            const indexElement = this.form.elementList.findIndex(e => e.id === this.newElement.id);
            if (indexElement !== -1) {
                this.form.elementList[indexElement] = JSON.parse(JSON.stringify(this.newElement));
            }

        } else {
            this.newElement.id = Tools.randomId();
            this.form.elementList.push(JSON.parse(JSON.stringify(this.newElement)));

        }
        this.updateForm();
        this.newElement = {
            newOptionList: [],
            dir: 'rtl',
        } as Element;
        console.log(this.form);


        this.hideModal('insertFieldsModal');
    }

    showModal(id) {
        $(`#${id}`).modal('show');
    }

    hideModal(id) {
        $(`#${id}`).modal('hide');
    }

    addOption() {
        if (!this.newOption.value) {
            alert('عنوان گزینه باید وارد شود.');
            return;
        }
        if (this.newOption.id) {
            const index = this.newElement.newOptionList.findIndex(option => option.id === this.newOption.id);
            if (index !== -1) {
                this.newElement.newOptionList[index] = JSON.parse(JSON.stringify(this.newOption));
            }
        } else {
            this.newOption.id = Tools.randomId();
            this.newElement.newOptionList.push(JSON.parse(JSON.stringify(this.newOption)));
            console.log(this.newElement.newOptionList);
        }
        this.newOption = {
            value: '',
            id: ''
        } as Option;
    }

    selectOptionForEdit(option: Option) {
        this.newOption = JSON.parse(JSON.stringify(option));
    }

    selectElementForEdit(element: Element) {
        this.newElement = JSON.parse(JSON.stringify(element));
        this.showModal('insertFieldsModal');

    }

    updateForm() {
        if (this.loadingSubmit) {
            return;
        }

        this.submitted = true;

        if (this.form.elementList.length === 0) {
            alert(' حداقل یک سوال وارد شود.');
            return;
        }

        this.subscription$.push(this.formService.update(this.form, this.form.id).subscribe());
        alert(' با موفقیت ذخیره شد.');

        if (this.newElement.newElementType === ElementType.DATE ||
            this.newElement.newElementType === ElementType.DATE_RANGE) {
            let rangeSelector = false;
            if (this.newElement.newElementType === ElementType.DATE_RANGE) {
                rangeSelector = true;
            }

            this.setDateJquery(this.newElement.id, rangeSelector);

        }
        // this.loadingSubmit = true;
        // this.loadingSubmit = false;


    }

    ngAfterViewInit(): void {
        for (const element of this.form.elementList) {
            if (element.newElementType === ElementType.DATE ||
                element.newElementType === ElementType.DATE_RANGE) {
                let rangeSelector = false;
                if (element.newElementType === ElementType.DATE_RANGE) {
                    rangeSelector = true;
                }
                this.setDateJquery(element.id, rangeSelector);
            }
        }
    }


    setDateJquery(id, rangeSelectorValue) {

        setTimeout(e1 => {
            $('#inputDate' + id).azPersianDateTimePicker({
                Placement: 'left', // default is 'bottom'
                Trigger: 'focus', // default is 'focus',
                enableTimePicker: false, // default is true,
                TargetSelector: '', // default is empty,
                GroupId: '', // default is empty,
                ToDate: false, // default is false,
                FromDate: false, // default is false,
                targetTextSelector: $('#inputDate' + id),
                rangeSelector: rangeSelectorValue

            }).on('change', (e) => {

            });

        }, 100);

    }

    elementMove(type: string, elementIndex: number) {
        const thisElement = JSON.parse(JSON.stringify(this.form.elementList[elementIndex]));
        if (type === 'up') {
            this.form.elementList[elementIndex] = JSON.parse(JSON.stringify(this.form.elementList[elementIndex - 1]));
            this.form.elementList[elementIndex - 1] = thisElement;
        } else if (type === 'down') {
            this.form.elementList[elementIndex] = JSON.parse(JSON.stringify(this.form.elementList[elementIndex + 1]));
            this.form.elementList[elementIndex + 1] = thisElement;
        }

    }

    back() {
        this.location.back();
    }

    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }
}

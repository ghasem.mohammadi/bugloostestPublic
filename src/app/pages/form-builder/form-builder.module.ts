import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormBuilderRoutingModule} from './form-builder-routing.module';
import {FormListComponent} from './form-list/form-list.component';
import {FormAnswerComponent} from './form-answer/form-answer.component';
import {UpsertFormComponent} from './action/upsert-form/upsert-form.component';
import {FormInformationComponent} from './action/upsert-form/form-information/form-information.component';
import {FormFieldsComponent} from './action/upsert-form/form-fields/form-fields.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgxMaskModule} from 'ngx-mask';


@NgModule({
    declarations: [
        FormListComponent,
        FormAnswerComponent,
        UpsertFormComponent,
        FormInformationComponent,
        FormFieldsComponent],
    exports: [
        FormAnswerComponent
    ],
    imports: [
        CommonModule,
        FormBuilderRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        NgxMaskModule.forRoot(),


    ]
})
export class FormBuilderModule {
}

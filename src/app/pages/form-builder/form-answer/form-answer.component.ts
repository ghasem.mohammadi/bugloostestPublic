import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {Form} from '../model/form';
import {ElementType} from '../model/enum/element-type';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Element} from '../model/element';
import {DateInputObject, FormData, QuestionAnswer} from '../model/formData';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '../service/form.service';
import {Subscription} from 'rxjs';

declare var $: any;

@Component({
    selector: 'app-form-answer',
    templateUrl: './form-answer.component.html',
    styleUrls: ['./form-answer.component.scss']
})
export class FormAnswerComponent implements OnInit, AfterViewInit, OnDestroy {


    @Input() form = {
        id: '',
        elementList: []
    } as Form;
    @Input() formData: FormData;
    @Input() disabledFiled: boolean;
    @Input() isView: boolean;
    @Input() formId: string;
    @Output() formDataOut = new EventEmitter<FormData>();
    @Output() backEmit = new EventEmitter<boolean>();

    htmlForm: FormGroup;


    dateInputObjectList: DateInputObject[] = [];
    dateInputObjectListCopy: DateInputObject[] = [];

    loadingSubmit = false;
    newElementType = ElementType;
    private subscription$: Subscription[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                private formService: FormService,
    ) {
        this.activatedRoute.queryParams.subscribe((params) => {
            params.formId ? this.formId = params.formId : '';
        });
    }

    ngOnInit(): void {
        this.getOne();
    }

    getOne() {
        this.subscription$.push(this.formService.getOne().subscribe((res) => {
            console.log(res);
            const index = res.findIndex(d => d.id === this.formId);
            if (index !== -1) {
                this.form = res[index];
                const formData = JSON.parse(localStorage.getItem('formData'));
                if (formData) {
                    this.formData = formData;
                }
                this.creatForm();
            }
        }));
    }

    creatForm() {
        this.htmlForm = new FormGroup({});
        let isFormData = false;
        if (this.formData) {
            isFormData = true;
        } else {
            this.formData = Object(FormData);
            isFormData = false;
        }
        for (const element of this.form.elementList) {

            this.creatFormByElement(element, isFormData);
        }
    }

    creatFormByElement(element: Element, isFormData) {
        console.log(element);

        if (isFormData) {
            if (element.newElementType === ElementType.TIME) {
                const hour = this.formData.answerList.find(answer => answer.questionId === element.id).answerIdList[0].hour;
                const min = this.formData.answerList.find(answer => answer.questionId === element.id).answerIdList[0].min;
                const name = element.newElementType + '*' + element.id + '*';
                this.htmlForm.addControl(name + 'min', new FormControl(min));
                this.htmlForm.addControl(name + 'hour', new FormControl(hour));
                if (element.required) {
                    this.htmlForm.controls[name + 'min'].setValidators(Validators.required);
                    this.htmlForm.controls[name + 'hour'].setValidators(Validators.required);
                }
            } else if (element.newElementType === ElementType.CHECK_BOX) {
                for (const option of element.newOptionList) {
                    const value = this.formData.answerList.find(answer =>
                        answer.questionId === element.id).answerIdList.some(id => id === option.id);
                    const name = element.newElementType + '*' + element.id + '*' + option.id;

                    this.htmlForm.addControl(name, new FormControl(value));
                }

            } else {
                const value = this.formData.answerList.find(answer => answer.questionId === element.id).answerIdList[0];
                const name = element.newElementType + '*' + element.id;
                this.htmlForm.addControl(name, new FormControl(value));

                if (element.required) {
                    this.htmlForm.controls[name].setValidators(Validators.required);
                }
                if (element.maxLength) {
                    this.htmlForm.controls[name].setValidators(Validators.maxLength(element.maxLength));
                }
                if (element.minLength) {
                    this.htmlForm.controls[name].setValidators(Validators.minLength(element.minLength));
                }
                if (element.required) {
                    this.htmlForm.addControl(name, new FormControl(value, Validators.required));
                }
                if (element.pattern) {
                    this.htmlForm.controls[name].setValidators(Validators.pattern(element.pattern));
                }
                if (element.newElementType === ElementType.DATE ||
                    element.newElementType === ElementType.DATE_RANGE) {
                    const newDateInputObject: DateInputObject = {
                        id: element.id,
                        date: value
                    };

                    if (!this.dateInputObjectList.some(e => e.id === newDateInputObject.id)) {
                        this.dateInputObjectList.push(newDateInputObject);
                    }
                }
            }
        } else {
            if (element.newElementType === ElementType.TIME) {
                const name = element.newElementType + '*' + element.id + '*';
                this.htmlForm.addControl(name + 'min', new FormControl(null));
                this.htmlForm.addControl(name + 'hour', new FormControl(null));
                if (element.required) {
                    this.htmlForm.controls[name + 'min'].setValidators(Validators.required);
                    this.htmlForm.controls[name + 'hour'].setValidators(Validators.required);
                }
            } else if (element.newElementType === ElementType.CHECK_BOX) {
                for (const option of element.newOptionList) {
                    const name = element.newElementType + '*' + element.id + '*' + option.id;
                    this.htmlForm.addControl(name, new FormControl(false));
                }

            } else {
                const name = element.newElementType + '*' + element.id;
                this.htmlForm.addControl(name, new FormControl(null));
                if (element.required) {
                    this.htmlForm.controls[name].setValidators(Validators.required);
                }
                if (element.maxLength) {
                    this.htmlForm.controls[name].setValidators(Validators.maxLength(element.maxLength));
                }
                if (element.minLength) {
                    this.htmlForm.controls[name].setValidators(Validators.minLength(element.minLength));
                }
                if (element.pattern) {
                    this.htmlForm.controls[name].setValidators(Validators.pattern(element.pattern));
                }
            }
        }

        this.dateInputObjectListCopy = JSON.parse(JSON.stringify(this.dateInputObjectList));
        if (this.disabledFiled || this.isView) {
            this.htmlForm.disable();
        }


    }


    ngAfterViewInit(): void {

        for (const element of this.form.elementList) {
            if (element.newElementType === ElementType.DATE ||
                element.newElementType === ElementType.DATE_RANGE
            ) {
                const newDateInputObject: DateInputObject = {
                    id: element.id,
                    date: null
                };
                if (!this.dateInputObjectList.some(e => e.id === newDateInputObject.id)) {
                    this.dateInputObjectList.push(newDateInputObject);
                }
                this.setDateJquery(element);

            }
        }
        this.dateInputObjectListCopy = JSON.parse(JSON.stringify(this.dateInputObjectList));

    }

    setDateJquery(element: Element) {
        console.log(element);
        let rangeSelectorValue = false;
        if (element.newElementType === ElementType.DATE_RANGE) {
            rangeSelectorValue = true;
        }
        setTimeout(e1 => {
            $('#inputDate' + element.id + this.form.id).azPersianDateTimePicker({
                Placement: 'left', // default is 'bottom'
                Trigger: 'click', // default is 'focus',
                enableTimePicker: false, // default is true,
                TargetSelector: '', // default is empty,
                GroupId: '', // default is empty,
                ToDate: false, // default is false,
                FromDate: false, // default is false,
                targetTextSelector: $('#inputDate' + element.id + this.form.id),
                rangeSelector: rangeSelectorValue

            }).on('change', (e) => {
                const val = $(e.currentTarget).val();
                const index = this.dateInputObjectList.findIndex(d => d.id === element.id);
                if (index !== -1) {
                    this.dateInputObjectList[index].date = val;
                }
                // $('#inputDate' + element.id).val(val).trigger('change');
                console.log(e);
            });

        }, 100);

    }

    onSubmitForm() {

        if (!this.htmlForm.dirty && JSON.stringify(this.dateInputObjectList) === JSON.stringify(this.dateInputObjectListCopy)) {
            alert('تغییری اعمال نشده است.');
            return;
        }
        // if (this.loadingSubmit) {
        //     return;
        // }
        // this.loadingSubmit = true;

        const formData = {
            formId: this.form.id,
            answerList: []
        } as FormData;
        for (const element of this.form.elementList) {
            const questionAnswer: QuestionAnswer = {
                questionId: element.id,
                questionElementType: element.newElementType,
                answerIdList: []
            };
            if (element.newElementType === ElementType.TIME) {
                const hour = this.htmlForm.controls[element.newElementType + '*' + element.id + '*' + 'hour'].value;
                const min = this.htmlForm.controls[element.newElementType + '*' + element.id + '*' + 'min'].value;
                if (element.required) {
                    if (!hour) {
                        alert(' ساعت برای ' + element.questionTitle + ' وارد شود.  ');
                        this.loadingSubmit = false;
                        return;
                    }
                    if (!min) {
                        alert(' دقیقه برای  ' + element.questionTitle + 'وارد شود. ');
                        this.loadingSubmit = false;
                        return;
                    }
                }
                const answerTime = {
                    hour,
                    min
                };
                if (answerTime.hour > 23 || answerTime.hour < 0) {
                    alert(' ساعت برای ' + element.questionTitle + ' صحیح وارد شود.  ');
                    this.loadingSubmit = false;
                    return;

                }
                if (answerTime.min > 59 || answerTime.min < 0) {
                    alert(' دقیقه برای ' + element.questionTitle + ' صحیح وارد شود.  ');
                    this.loadingSubmit = false;
                    return;

                }
                questionAnswer.answerIdList.push(answerTime);
            } else if (element.newElementType === ElementType.CHECK_BOX) {
                const selectedList = [];
                for (const option of element.newOptionList) {
                    const value = this.htmlForm.controls[element.newElementType + '*' + element.id + '*' + option.id].value;
                    if (value === true) {
                        selectedList.push(option.id);
                    }
                }
                if (element.required && selectedList.length === 0) {
                    alert(' گزینه ای  برای ' + element.questionTitle + ' انتخاب  شود. ');

                    this.loadingSubmit = false;
                    return;
                }
                if (element.maxItemSelectable < selectedList.length) {
                    alert('   گزینه های انتخاب شده  برای ' + element.questionTitle + '  بیشتر از حد مجاز است.');

                    this.loadingSubmit = false;
                    return;
                }
                if (element.minItemSelectable > selectedList.length) {
                    alert('  گزینه های انتخاب شده  برای ' + element.questionTitle + '  کمتر از حد مجاز است. ');

                    this.loadingSubmit = false;
                    return;
                }

                questionAnswer.answerIdList = selectedList;
                // if (element.required) {
                //     this.htmlForm.addControl(element.newElementType +'_ +element.id, new FormControl(null, Validators.required));
                // } else {
                //
                // }
            } else if (element.newElementType === ElementType.DATE
                || element.newElementType === ElementType.DATE_RANGE) {
                const value = this.dateInputObjectList.find(d => d.id === element.id).date;
                if (element.required) {
                    if (!value) {
                        alert(' تاریخ برای   ' + element.questionTitle + ' وارد شود. ');
                        this.loadingSubmit = false;
                        return;
                    }
                }
                if (value) {
                    questionAnswer.answerIdList.push(value);
                }

            } else {
                const value = this.htmlForm.controls[element.newElementType + '*' + element.id].value;
                if (element.required) {
                    if (value === null || value === undefined) {
                        alert('   مقدار برای ' + element.questionTitle + ' وارد شود.  ');
                        this.loadingSubmit = false;
                        return;
                    }
                }
                if (value) {
                    questionAnswer.answerIdList.push(value);
                }


            }
            formData.answerList.push(questionAnswer);

        }
        console.log(formData);
        this.formDataOut.emit(formData);

    }


    ngOnDestroy(): void {
        this.subscription$.forEach((subscription) => subscription.unsubscribe());

    }


    back() {
        this.backEmit.emit(true);
    }
}

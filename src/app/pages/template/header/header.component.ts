import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../../../shared/dataService/dataServise';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    isLogin = true;
    token = localStorage.getItem('token');
    activeMenuItem = 'formBuilder';

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        this.checkIsLogin();
    }

    checkIsLogin() {
        if (localStorage.getItem('token')) {
            DataService.setIsLogin(true);

        }
        DataService.getIsLogin.subscribe(res => {
            this.isLogin = res;
            console.log(res);
        });
    }

    signout() {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
        DataService.setIsLogin(false);

    }


}

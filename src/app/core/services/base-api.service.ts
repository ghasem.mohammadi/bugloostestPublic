import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BaseApiService {
  baseServerUrl: string;

  constructor(private http: HttpClient) {
    this.baseServerUrl = '';
  }

  get<T>(url: string, params: any = null): Observable<T> {
    // if (params) {
    //   let httpParams = new HttpParams();
    //   Object.keys(params).forEach((key) => {
    //     httpParams = httpParams.append(key, params[key]);
    //   });
    // } else {
    //   params = new HttpParams();
    // }
    let key = url;
    if (params) {
      key = url + params;
    }
    const res = new BehaviorSubject<any>(null);
    res.next(JSON.parse(localStorage.getItem(key)));
    return res.asObservable();
    // return this.http.get<T>(`${this.baseServerUrl}${url}`, {

    //   headers: this.headers,
    //   params,
    // });
  }

  post<T, D>(url: string, data: D, params: any = null, resType?: any): Observable<T> {
    console.log('post');
    // if (params) {
    //   let httpParams = new HttpParams();
    //   Object.keys(params).forEach((key) => {
    //     httpParams = httpParams.append(key, params[key]);
    //   });
    // } else {
    //   params = new HttpParams();
    // }
    let key = url;
    if (params) {
      key = url + params;
    }
    let list: any[] = JSON.parse(localStorage.getItem(key));
    console.log('list', list);
    if (list) {
      list.push(data);
    } else {
      list = [data];
    }
    console.log('list2222', list);
    localStorage.setItem(key, JSON.stringify(list));
    const res = new BehaviorSubject<any>(null);
    res.next(true);
    return res.asObservable();
    // return this.http.post<T>(`${this.baseServerUrl}${url}`, data, {
    //   responseType: resType,
    //   headers: this.headers,
    //   params,
    //
    // });
  }

  put<T, D>(url: string, data: D, params: any = null): Observable<T> {
    console.log('put');
    const list: any[] = JSON.parse(localStorage.getItem(url));
    if (list) {
      const index = list.findIndex(e => e.id === params);
      list[index] = data;
    }
    localStorage.setItem(url, JSON.stringify(list));
    const res = new BehaviorSubject<any>(null);
    res.next(true);
    return res.asObservable();
    // return this.http.put<T>(`${this.baseServerUrl}${url}`, data, {
    //   headers: this.headers,
    //   params,
    // });
  }

  delete<T>(url: string, params: any = null): Observable<T> {
    const list: any[] = JSON.parse(localStorage.getItem(url));
    if (list) {
      const index = list.findIndex(e => e.id === params);
      list.splice(index, 1);
    }
    localStorage.setItem(url, JSON.stringify(list));
    const res = new BehaviorSubject<any>(null);
    res.next(true);
    return res.asObservable();
    // return this.http.delete<T>(`${this.baseServerUrl}${url}`, {
    //   headers: this.headers,
    //   params,
    // });
  }

  get headers(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };

    return new HttpHeaders(headersConfig);
  }

}

import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {delay, retryWhen, scan, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class ForbiddenInterceptor implements HttpInterceptor {
    constructor(public _router: Router) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            retryWhen((errors) => {
                return errors.pipe(
                    scan((errorCount, err) => {
                        if (err.status === 404) {
                            alert('404');
                        }
                        if (err.status !== 401) {
                            throw err;
                        }
                        if (errorCount === 2) {
                            throw err;
                        }
                        return errorCount + 1;
                    }, 1),
                    delay(4000),
                );
            }),
            tap(
                {
                    error: (error: any) => {
                        console.log(error);
                        if (error.status === 401) {
                            localStorage.removeItem('token');
                            this._router.navigate(['/']);
                        }
                    }
                }
            )
        );
    }

}

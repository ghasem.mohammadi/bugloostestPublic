import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PanelGuardService implements CanActivate {
  constructor(private router: Router) {
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('token')) {
      return true;
    }

    // not logged in so redirect to login page with the return url
    return this.router.createUrlTree(['/'], {queryParams: {returnUrl: state.url}});
  }
}
